/*
 * Chess.h
 *
 *  Created on: Sep 20, 2014
 *      Author: shaitrinczer
 */
#include<stdio.h>

#ifndef CHESS_H_
#define CHESS_H_

#define WHITE_PAWN 'm'
#define BLACK_PAWN 'M'
#define WHITE_BISHOP 'b'
#define BLACKE_BISHOP 'B'
#define WHITE_ROOK 'r'
#define BLACK_BISHOP 'R'
#define WHITE_KNIGHT 'n'
#define BLACK_KNIGHT 'N'
#define WHITE_K 'k'
#define BLACK_K 'K'
#define EMPTY ' '




#endif /* CHESS_H_ */
